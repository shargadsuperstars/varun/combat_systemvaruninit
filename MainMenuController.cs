﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public AudioSource sound;
   public void PlayGame()
   {
       SceneManager.LoadScene("TownOfNakkisarie");
   }

   public void EndGame()
   {
       Application.Quit();
   }

   public void Mute() {
       {
           AudioListener.pause = !AudioListener.pause;
       }
   }

   public void GameRestart()
   {
      StartCoroutine(RestartGame());
   }
   IEnumerator RestartGame()
    {
        yield return new WaitForSecondsRealtime(1f);
        SceneManager.LoadScene("SampleScene");
    }
    public void Mymenu()
   {
       SceneManager.LoadScene("MainScene");
   }

    public void Maps()
   {
       SceneManager.LoadScene("Map");
   }

    public void Tavern()
   {
    sound.Play();
    StartCoroutine("Music");
   }
    public void Tavern2()
   {
    SceneManager.LoadScene("Tavern 2");
   }

    public void NotTavern()
   {
       
       SceneManager.LoadScene("Tavern 1");
   }
   IEnumerator Music()
    {
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene("Tavern");
    }
   public void Arena()
   {
       SceneManager.LoadScene("Arena");
   }
    public void House()
   {
       SceneManager.LoadScene("House");
   }

   public void House1()
   {
       SceneManager.LoadScene("House 1");
   }
    public void TownOfNakkisarie()
   {
    SceneManager.LoadScene("TownOfNakkisarie");
   }
   public void TownOfNakkisarie1()
   {
    SceneManager.LoadScene("TownOfNakkisarie 1");
   }
   public void TownOfNakkisarie2()
   {
    SceneManager.LoadScene("TownOfNakkisarie 2");
   }
    public void TownOfNakkisarieNot()
   {
    SceneManager.LoadScene("TownOfNakkisarieNOT");
   }
  public void TownOfNakkisarieNot1()
   {
    SceneManager.LoadScene("TownOfNakkisarieNOT 1");
   }
   public void SetDiffculty()
   {
    SceneManager.LoadScene("SetDifficulty");
   }

   public void Market()
   {
    SceneManager.LoadScene("MarketPlace");
   }
    public void Market1()
   {
    SceneManager.LoadScene("MarketPlace 1");
   }
     public void Shop1()
   {
    SceneManager.LoadScene("Shop1");
   }
 public void House2()
   {
    SceneManager.LoadScene("House 2");
   }
public void Tavern3()
   {
    SceneManager.LoadScene("Tavern 3");
   }

   public void Draw()
   {
    SceneManager.LoadScene("ExampleDrawingScene");
   }
    public void Draw1()
   {
    SceneManager.LoadScene("ExampleDrawingScene 1");
   }
    public void Draw2()
   {
    SceneManager.LoadScene("ExampleDrawingScene 2");
   }
    public void Draw3()
   {
    SceneManager.LoadScene("ExampleDrawingScene 3");
   }
    public void Draw4()
   {
    SceneManager.LoadScene("ExampleDrawingScene 4");
   }

   public void Quest1()
   {
    SceneManager.LoadScene("Playable Demo Scene - Light");
   }

    public void RestartCombat()
   {
    SceneManager.LoadScene("Battle 0");
   }
}
