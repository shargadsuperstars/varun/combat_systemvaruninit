﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Positiondetect : MonoBehaviour
{
    private string name1;
    private int pos1;
    private int n;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    
    public void posfun(int pos)
    {
        n=pos;
        //Debug.Log(pos);
    }
    public void namefun(string name)
    {
        Debug.Log("Name:"+name+"Position:"+n);
        if (name=="Tiger")
        {
            if (n==0)
            {
               FindObjectOfType<positionreset>().t_move_r0();  
            } 
            if (n==1)
            {
               FindObjectOfType<positionreset>().t_move_r1();  
            } 
            if (n==2)
            {
               FindObjectOfType<positionreset>().t_move_r2();  
            } 
            if (n==3)
            {
               FindObjectOfType<positionreset>().t_move_r3();  
            }
            if (n==4)
            {
               FindObjectOfType<positionreset>().t_move_r4();  
            }
            if (n==5)
            {
               FindObjectOfType<positionreset>().t_move_r5();  
            }
        }
        
        if (name=="goblin")
        {
            if (n==0)
            {
               FindObjectOfType<positionreset>().g_move_r0();  
            } 
            if (n==1)
            {
               FindObjectOfType<positionreset>().g_move_r1();  
            } 
            if (n==2)
            {
               FindObjectOfType<positionreset>().g_move_r2();  
            } 
            if (n==3)
            {
               FindObjectOfType<positionreset>().g_move_r3();  
            }
            if (n==4)
            {
               FindObjectOfType<positionreset>().g_move_r4();  
            }
            if (n==5)
            {
               FindObjectOfType<positionreset>().g_move_r5();  
            }
        }
         if (name=="bull")
        {
            if (n==0)
            {
               FindObjectOfType<positionreset>().b_move_r0();  
            } 
            if (n==1)
            {
               FindObjectOfType<positionreset>().b_move_r1();  
            } 
            if (n==2)
            {
               FindObjectOfType<positionreset>().b_move_r2();  
            } 
            if (n==3)
            {
               FindObjectOfType<positionreset>().b_move_r3();  
            }
            if (n==4)
            {
               FindObjectOfType<positionreset>().b_move_r4();  
            }
            if (n==5)
            {
               FindObjectOfType<positionreset>().b_move_r5();  
            }
        }
    }
}
