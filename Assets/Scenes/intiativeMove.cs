﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class intiativeMove : MonoBehaviour
{
    public GameObject p1;
    public GameObject p2;
    public GameObject p3;

    private int p1_inti=6;
    private int p2_inti=9;
    private int p3_inti=8;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (p1_inti>p2_inti && p1_inti>p3_inti)
        {
            p1.SetActive(true);
        }
        if (p2_inti>p1_inti && p2_inti>p3_inti)
        {
            p2.SetActive(true);
        }
        if (p3_inti>p1_inti && p3_inti>p2_inti)
        {
            p3.SetActive(true);
        }
    }
}
