﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class DragDrop2 : MonoBehaviour, IPointerDownHandler , IBeginDragHandler, IEndDragHandler, IDragHandler
{
[SerializeField] private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private string A = "goblin";
    void Awake() {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
     public void OnBeginDrag(PointerEventData eventData)
  {
      //Debug.Log("OnBeginDrag");
      canvasGroup.blocksRaycasts = false;
  }
     public void OnDrag(PointerEventData eventData)
  {
      //Debug.Log("OnDrag");
      rectTransform.anchoredPosition += eventData.delta;
  }
  public void OnEndDrag(PointerEventData eventData)
  {
      FindObjectOfType<Positiondetect>().namefun(A);
      canvasGroup.blocksRaycasts = true;
  }
  public void OnPointerDown(PointerEventData eventData)
  {
      //Debug.Log("OnPointerDown");
  }
}
