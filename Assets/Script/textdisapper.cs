﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textdisapper : MonoBehaviour
{
public Animator anim1;
public GameObject text1;
public GameObject text2;
    void Start()
    {
        anim1 = GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        if (!text1.activeSelf)
        {
        StartCoroutine("textr1");
        StartCoroutine("texta1");
        }
    IEnumerator textr1()
    {
        yield return new WaitForSeconds(1f);
        text2.SetActive(true);
        anim1.Play("text2");
        StopCoroutine("textr1");
    }
    IEnumerator texta1()
    {
        yield return new WaitForSeconds(3f);
        text2.SetActive(false);
    }
}
}
