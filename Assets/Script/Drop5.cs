﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drop5 : MonoBehaviour ,IDropHandler
{
    private int B=5;   
    public void OnDrop(PointerEventData eventData)
 {
     FindObjectOfType<Positiondetect>().posfun(B);
      if (eventData.pointerDrag!= null)
      {
          eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
      }
 }
}
