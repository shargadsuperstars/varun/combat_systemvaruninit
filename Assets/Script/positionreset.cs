﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class positionreset : MonoBehaviour
{
    public GameObject p1_r0;
    public GameObject p1_r1;
    public GameObject p1_r2;
    public GameObject p1_r3;
    public GameObject p1_r4;
    public GameObject p1_r5;
    public GameObject p2_r0;
    public GameObject p2_r1;
    public GameObject p2_r2;
    public GameObject p2_r3;
    public GameObject p2_r4;
    public GameObject p2_r5;
    public GameObject p3_r0;
    public GameObject p3_r1;
    public GameObject p3_r2;
    public GameObject p3_r3;
    public GameObject p3_r4;
    public GameObject p3_r5;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    // Update is called once per frame
    public void t_move_r0()
    {
       int n=111;
    FindObjectOfType<moveplayer2>().movein2(n);
      //p1_r0.layer =0;
    p1_r1.SetActive(false);
    p1_r3.SetActive(false);
    p1_r2.SetActive(false);
    p1_r4.SetActive(false);
    p1_r5.SetActive(false);
    p1_r0.SetActive(true);
    }

    public void t_move_r1()
    {
       int n=112;
    FindObjectOfType<moveplayer2>().movein2(n);
      // p1_r1.layer =1;
    p1_r1.SetActive(true);
    p1_r3.SetActive(false);
    p1_r2.SetActive(false);
    p1_r4.SetActive(false);
    p1_r5.SetActive(false);
    p1_r0.SetActive(false);
    }

    public void t_move_r2()
    {
       int n=113;
     FindObjectOfType<moveplayer2>().movein2(n);
      //p1_r2.layer =2;
    p1_r1.SetActive(false);
    p1_r3.SetActive(false);
    p1_r2.SetActive(true);
    p1_r4.SetActive(false);
    p1_r5.SetActive(false);
    p1_r0.SetActive(false);
    }

    public void t_move_r3()
    {
       int n=114;
     FindObjectOfType<moveplayer2>().movein2(n);
      //p1_r3.layer =5;
    p1_r1.SetActive(false);
    p1_r3.SetActive(true);
    p1_r2.SetActive(false);
    p1_r4.SetActive(false);
    p1_r5.SetActive(false);
    p1_r0.SetActive(false);
    }

    public void t_move_r4()
    {
       int n=115;
     FindObjectOfType<moveplayer2>().movein2(n);
     // p1_r4.layer =4;
    p1_r1.SetActive(false);
    p1_r3.SetActive(false);
    p1_r2.SetActive(false);
    p1_r4.SetActive(true);
    p1_r5.SetActive(false);
    p1_r0.SetActive(false);
    }

    public void t_move_r5()
    {
       int n=116;
     FindObjectOfType<moveplayer2>().movein2(n);
       //p1_r5.layer =3;
    p1_r1.SetActive(false);
    p1_r3.SetActive(false);
    p1_r2.SetActive(false);
    p1_r4.SetActive(false);
    p1_r5.SetActive(true);
    p1_r0.SetActive(false);
    }
      public void g_move_r0()
    {
    int n=1;
    FindObjectOfType<moveplayer>().movein(n);
    //p2_r0.layer =0;
    p2_r1.SetActive(false);
    p2_r3.SetActive(false);
    p2_r2.SetActive(false);
    p2_r4.SetActive(false);
    p2_r5.SetActive(false);
    p2_r0.SetActive(true);
    }

    public void g_move_r1()
    {
      int n=2;
    FindObjectOfType<moveplayer>().movein(n);
    //p2_r1.layer =0;
    p2_r1.SetActive(true);
    p2_r3.SetActive(false);
    p2_r2.SetActive(false);
    p2_r4.SetActive(false);
    p2_r5.SetActive(false);
    p2_r0.SetActive(false);
    }

    public void g_move_r2()
    {
      int n=3;
    FindObjectOfType<moveplayer>().movein(n);
    //p2_r2.layer =2;
    p2_r1.SetActive(false);
    p2_r3.SetActive(false);
    p2_r2.SetActive(true);
    p2_r4.SetActive(false);
    p2_r5.SetActive(false);
    p2_r0.SetActive(false);
    }

    public void g_move_r3()
    {
      int n=4;
    FindObjectOfType<moveplayer>().movein(n);
      //p2_r3.layer =5;
    p2_r1.SetActive(false);
    p2_r3.SetActive(true);
    p2_r2.SetActive(false);
    p2_r4.SetActive(false);
    p2_r5.SetActive(false);
    p2_r0.SetActive(false);
    }

    public void g_move_r4()
    {
      int n=5;
    FindObjectOfType<moveplayer>().movein(n);
      //p2_r4.layer =4;
    p2_r1.SetActive(false);
    p2_r3.SetActive(false);
    p2_r2.SetActive(false);
    p2_r4.SetActive(true);
    p2_r5.SetActive(false);
    p2_r0.SetActive(false);
    }

    public void g_move_r5()
    {
      int n=6;
    FindObjectOfType<moveplayer>().movein(n);
      // p2_r5.layer =3;
    p2_r1.SetActive(false);
    p2_r3.SetActive(false);
    p2_r2.SetActive(false);
    p2_r4.SetActive(false);
    p2_r5.SetActive(true);
    p2_r0.SetActive(false);
    }
      public void b_move_r0()
    {
       int n=11;
    FindObjectOfType<moveplayer1>().movein1(n);
     // p3_r0.layer =0;
    p3_r1.SetActive(false);
    p3_r3.SetActive(false);
    p3_r2.SetActive(false);
    p3_r4.SetActive(false);
    p3_r5.SetActive(false);
    p3_r0.SetActive(true);
    }

    public void b_move_r1()
    {
       int n=12;
    FindObjectOfType<moveplayer1>().movein1(n);
     // p3_r1.layer =0;
    p3_r1.SetActive(true);
    p3_r3.SetActive(false);
    p3_r2.SetActive(false);
    p3_r4.SetActive(false);
    p3_r5.SetActive(false);
    p3_r0.SetActive(false);
    }

    public void b_move_r2()
    {
       int n=13;
    FindObjectOfType<moveplayer1>().movein1(n);
     //p3_r2.layer =2; 
    p3_r1.SetActive(false);
    p3_r3.SetActive(false);
    p3_r2.SetActive(true);
    p3_r4.SetActive(false);
    p3_r5.SetActive(false);
    p3_r0.SetActive(false);
    }

    public void b_move_r3()
    {
       int n=14;
    FindObjectOfType<moveplayer1>().movein1(n);
      //p3_r3.layer =5;
    p3_r1.SetActive(false);
    p3_r3.SetActive(true);
    p3_r2.SetActive(false);
    p3_r4.SetActive(false);
    p3_r5.SetActive(false);
    p3_r0.SetActive(false);
    }

    public void b_move_r4()
    {
       int n=15;
    FindObjectOfType<moveplayer1>().movein1(n);
     // p3_r4.layer =4;
    p3_r1.SetActive(false);
    p3_r3.SetActive(false);
    p3_r2.SetActive(false);
    p3_r4.SetActive(true);
    p3_r5.SetActive(false);
    p3_r0.SetActive(false);
    }

    public void b_move_r5()
    {
       int n=16;
    FindObjectOfType<moveplayer1>().movein1(n);
     // p3_r5.layer =3;
    p3_r1.SetActive(false);
    p3_r3.SetActive(false);
    p3_r2.SetActive(false);
    p3_r4.SetActive(false);
    p3_r5.SetActive(true);
    p3_r0.SetActive(false);
    }
}
