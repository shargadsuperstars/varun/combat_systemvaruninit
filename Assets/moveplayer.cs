﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveplayer : MonoBehaviour
{
    public GameObject p1_r0;
    public GameObject p1_r1;
    public GameObject p1_r2;
    public GameObject p1_r3;
    public GameObject p1_r4;
    public GameObject p1_r5;
    public GameObject p2_r0;
    public GameObject p2_r1;
    public GameObject p2_r2;
    public GameObject p2_r3;
    public GameObject p2_r4;
    public GameObject p2_r5;
    public GameObject p3_r0;
    public GameObject p3_r1;
    public GameObject p3_r2;
    public GameObject p3_r3;
    public GameObject p3_r4;
    public GameObject p3_r5;
    private int n1;
    private int n2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void movein(int n)
    {
        n1=n;
    }

    public void move()
    {
    //player1 p1
    if (n1==1)
    {
      if (p1_r0.activeSelf)
      {
         
         if ((!p2_r4.activeSelf)&&(!p3_r4.activeSelf))
         {
             p1_r4.SetActive(true);
             p1_r0.SetActive(false);
             n2=1;
         }
         if ((!p2_r5.activeSelf)&&(!p3_r5.activeSelf))
         {
               if (!p1_r4.activeSelf)
       {
             p1_r5.SetActive(true);
             p1_r0.SetActive(false);
             n2=1;
       }
         }
         if ((!p2_r3.activeSelf)&&(!p3_r3.activeSelf))
         {
              if (!p1_r4.activeSelf)
      {
           if (!p1_r5.activeSelf)
       {
             p1_r3.SetActive(true);
             p1_r0.SetActive(false);
             n2=1;
       }
      }
         }
      }
    }
    //player1 p2
    if (n1==2)
    {
    if (p1_r1.activeSelf)
      {
         
         if ((!p2_r4.activeSelf)&&(!p3_r4.activeSelf))
         {
             p1_r4.SetActive(true);
             p1_r1.SetActive(false);
             n2=1;
         }
         if ((!p2_r5.activeSelf)&&(!p3_r5.activeSelf))
         {
               if (!p1_r4.activeSelf)
       {
             p1_r5.SetActive(true);
             p1_r1.SetActive(false);
             n2=1;
       }
         }
         if ((!p2_r3.activeSelf)&&(!p3_r3.activeSelf))
         {
              if (!p1_r4.activeSelf)
      {
           if (!p1_r5.activeSelf)
       {
             p1_r3.SetActive(true);
             p1_r1.SetActive(false);
             n2=1;
       }
      }
         }
      }
    }

    //player1 p3
    if (n1==3)
    {
    if (p1_r2.activeSelf)
      {
         
         if ((!p2_r4.activeSelf)&&(!p3_r4.activeSelf))
         {
             p1_r4.SetActive(true);
             p1_r2.SetActive(false);
             n2=1;
         }
         if ((!p2_r5.activeSelf)&&(!p3_r5.activeSelf))
         {
               if (!p1_r4.activeSelf)
       {
             p1_r5.SetActive(true);
             p1_r2.SetActive(false);
             n2=1;
       }
         }
         if ((!p2_r3.activeSelf)&&(!p3_r3.activeSelf))
         {
              if (!p1_r4.activeSelf)
      {
           if (!p1_r5.activeSelf)
       {
             p1_r3.SetActive(true);
             p1_r2.SetActive(false);
             n2=1;
       }
      }
         }
      }
    }

      //player1 p4
      if (n1==4)
    {
    if (p1_r3.activeSelf)
      {
         
         if ((!p2_r1.activeSelf)&&(!p3_r1.activeSelf))
         {
             p1_r1.SetActive(true);
             p1_r3.SetActive(false);
             n2=1;
         }
         if ((!p2_r0.activeSelf)&&(!p3_r0.activeSelf))
         {
               if (!p1_r1.activeSelf)
       {
             p1_r0.SetActive(true);
             p1_r3.SetActive(false);
             n2=1;
       }
         }
         if ((!p2_r2.activeSelf)&&(!p3_r2.activeSelf))
         {
              if (!p1_r0.activeSelf)
      {
           if (!p1_r1.activeSelf)
       {
             p1_r2.SetActive(true);
             p1_r3.SetActive(false);
             n2=1;
       }
      }
         }
      }
    }

       //player1 p5
    if (n1==5)
    {
    if (p1_r4.activeSelf)
      {
         
         if ((!p2_r1.activeSelf)&&(!p3_r1.activeSelf))
         {
             p1_r1.SetActive(true);
             p1_r4.SetActive(false);
             n2=1;
         }
         if ((!p2_r0.activeSelf)&&(!p3_r0.activeSelf))
         {
               if (!p1_r1.activeSelf)
       {
             p1_r0.SetActive(true);
             p1_r4.SetActive(false);
             n2=1;
       }
         }
         if ((!p2_r2.activeSelf)&&(!p3_r2.activeSelf))
         {
              if (!p1_r0.activeSelf)
      {
           if (!p1_r1.activeSelf)
       {
             p1_r2.SetActive(true);
             p1_r4.SetActive(false);
             n2=1;
       }
      }
         }
      }
    }
        
     //player1 p6
    if (n1==6)
    {
    if (p1_r5.activeSelf)
      {
         
         if ((!p2_r1.activeSelf)&&(!p3_r1.activeSelf))
         {
             p1_r1.SetActive(true);
             p1_r5.SetActive(false);
             n2=1;
         }
         if ((!p2_r0.activeSelf)&&(!p3_r0.activeSelf))
         {
               if (!p1_r1.activeSelf)
       {
             p1_r0.SetActive(true);
             p1_r5.SetActive(false);
             n2=1;
       }
         }
         if ((!p2_r2.activeSelf)&&(!p3_r2.activeSelf))
         {
              if (!p1_r0.activeSelf)
      {
           if (!p1_r1.activeSelf)
       {
             p1_r2.SetActive(true);
             p1_r5.SetActive(false);
             n2=1;
       }
      }
         }
      }
    }
    }
}
